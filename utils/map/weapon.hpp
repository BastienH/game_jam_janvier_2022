#pragma once

#include "point.hpp"

namespace utils::map
{
    struct Weapon
    {
        unsigned short id{0};
        Point position{0, 0};

        Weapon(unsigned short id, const Point& position): id(id), position(position){}
    };
}