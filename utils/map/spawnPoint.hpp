#pragma once

#include "point.hpp"

namespace utils::map
{
    using SpawnPoint = Point;
}