#pragma once

#include <string>
#include <nlohmann/json.hpp>
#include "map.hpp"
#include <box2d/b2_world.h>

namespace utils::map
{
    namespace loader
    {
        Map load(const std::string& path);
    };
}