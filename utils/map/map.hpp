#pragma once

#include <vector>
#include <string>
#include <box2d/box2d.h>

#include "point.hpp"
#include "wall.hpp"
#include "weapon.hpp"
#include "spawnPoint.hpp"

namespace utils::map
{
    struct Map
    {
        std::vector<Wall> walls;
        std::vector<SpawnPoint> spawns;
        std::vector<Weapon> weapons;
        std::string name;
        float width, height;
    };
}