#pragma once

#include <box2d/box2d.h>

struct Point
{
    float x{0};
    float y{0};

    operator b2Vec2() const { return b2Vec2{x, y}; }

    void move(const Point& d)
    {
        x += d.x;
        y += d.y;
    }
};
