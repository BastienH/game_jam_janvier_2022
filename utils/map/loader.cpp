#include "loader.hpp"
#include <fstream>
#include <algorithm>

namespace utils::map::loader
{
    Map load(const std::string& path)
    {
        constexpr float px_to_m{10};

        nlohmann::json value;

        std::ifstream input(path);
        input >> value;

        Map map;

        // Parcourt du JSON
        map.name = value["properties"]["name"].get<std::string>();
        map.width = value["properties"]["width"].get<float>()/px_to_m;
        map.height = value["properties"]["height"].get<float>()/px_to_m;
        
        unsigned short id{0};
        
        std::transform(value["walls"].begin(), value["walls"].end(), std::back_inserter(map.walls), [&map, &id](const auto& wall)
        {
            b2PolygonShape polygon{};
            std::vector<Point> points{};

            std::for_each(wall.begin(), wall.end(), [&](const auto& point)
            {
                points.push_back({point["x"], point["y"]});
            });

            std::vector<b2Vec2> vertices(points.begin(), points.end());

            polygon.Set(vertices.data(), vertices.size());

            return Wall(id++, points, polygon);
        });

        std::transform(value["poi"]["player"].begin(), value["poi"]["player"].end(), std::back_inserter(map.spawns), [&map](const auto& spawnPoint)
        {
            return SpawnPoint{spawnPoint["x"], spawnPoint["y"]};
        });

        std::transform(value["poi"]["weapons"].begin(), value["poi"]["weapons"].end(), std::back_inserter(map.weapons), [&map](const auto& weapon)
        {
            return Weapon{weapon["id"], Point{weapon["x"], weapon["y"]}};
        });
        
        return map;
    }
}