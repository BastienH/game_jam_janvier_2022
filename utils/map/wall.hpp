#pragma once

#include <box2d/box2d.h>
#include <vector>
#include "point.hpp"

namespace utils::map
{
    struct Wall
    {
        unsigned short id{0};
        std::vector<Point> points{};
        b2PolygonShape polygon{};

        Wall(unsigned short id, const std::vector<Point>& points, const b2PolygonShape& polygon):
        id(id), points(points), polygon(polygon){



            
        }
    };
}