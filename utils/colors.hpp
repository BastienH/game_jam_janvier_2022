#pragma once

#include <array>
#include <cstdint>

namespace utils::colors {
    struct Color {
        std::uint8_t r;
        std::uint8_t g;
        std::uint8_t b;

        constexpr Color(std::uint8_t r, std::uint8_t g, std::uint8_t b) :
            r(r),
            g(g),
            b(b)
        {};
    };

    constexpr static inline Color red{255, 0, 0};
    constexpr static inline Color green{0, 255, 0};
    constexpr static inline Color blue{0, 0, 255};

    constexpr static inline std::array<Color, 3> colors{
        red, green, blue
    };
}