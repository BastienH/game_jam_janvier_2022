#include <atomic>

#include <common/sigaction.hpp>
#include <net/server.hpp>

std::atomic_bool running = false;

void server() {
	std::printf("starting server... ");
	net::Server server{4273};

	std::printf("ready on %s:%d\n", server.ipAddr().c_str(), server.port());

	running = true;

	try {
		while(running) {
			server.accept(
				[](net::Client client) {
					std::printf("client connected from %s:%d\n", client.ipAddr().c_str(), client.port());
					while(running) {
						auto data = client.recvStr();
						std::printf("%s", data.c_str());
						client.send(data);
					}
				}
			);
		}
	} catch(std::exception const& e) {
		if(running) throw e;
	}

	std::puts("disconnected");
}

int main() try {
	if(!net::init()) {
		std::fputs("cannot init network\n", stderr);
		return 1;
	}

	std::puts("setup signal interruption");
	auto old = common::sigaction(SIGINT, +[](int) { running = false; });
	server();
	common::sigactionRestore(SIGINT, old);

	std::puts("terminate");

	net::release();
} catch(std::exception const& e) {
	std::fprintf(stderr, "#### [net::Exception] ####\n%s\n", e.what());
}
