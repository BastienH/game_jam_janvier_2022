#include <atomic>
#include <net/client.hpp>

#include <common/sigaction.hpp>
#include <net/portability.hpp>

std::atomic_bool running = false;

void client() {
	std::printf("starting client... ");
	net::Client client{"localhost", 4273};

	std::printf("connected to %s:%d\n", client.ipAddr().c_str(), client.port());

	while(running) {
		auto data = client.recvStr();
		std::printf("%s", data.c_str());
		client.send(data);
	}

	std::puts("disconnected");
}

int main() try {
	if(!net::init()) {
		std::fputs("cannot init network\n", stderr);
		return 1;
	}

	std::puts("setup signal interruption");
	auto old = common::sigaction(SIGINT, +[](int) { running = false; });

	running = true;
	client();

	common::sigactionRestore(SIGINT, old);

	std::puts("terminate");

	net::release();
} catch(std::exception const& e) {
	std::fprintf(stderr, "#### [net::Exception] ####\n%s\n", e.what());
}
