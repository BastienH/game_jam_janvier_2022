#include <array>
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>

#include <common/sigaction.hpp>
#include <net/server.hpp>
#include <net/serialise.hpp>
#include <net/deserialise.hpp>

std::atomic_bool running = false;

std::mutex mRunning;
std::condition_variable cvRunning;

void server() {
	// Server
	std::printf("starting server... ");
	net::Server server{4273};

	std::printf("ready on %s:%d\n", server.ipAddr().c_str(), server.port());

	running = true;
	cvRunning.notify_one();

	while(running) {
		server.accept(
			[](net::Client client) {
				std::printf("client connected from %s:%d\n", client.ipAddr().c_str(), client.port());

				auto message = client.recvStr();
				client.send("ack");
				auto ndata = client.recv();

				net::Serialised<std::uint32_t> ns{{ndata[0], ndata[1], ndata[2], ndata[3]}};

				std::uint32_t n = net::deserialise<std::uint32_t>(ns);
				std::printf("%s%d\n", message.c_str(), n);
			}
		);
	}

	std::puts("disconnected");
}

void client(int n) {
	std::printf("starting client#%d... ", n);
	net::Client client{"localhost", 4273};

	std::printf("connected (%s:%d)\n", client.ipAddr().c_str(), client.port());

	client.send("Hello from client#");
	static_cast<void>(client.recv());
	client.send(net::serialise(std::uint32_t(n)));
}

int main() try {
	if(!net::init()) {
		std::fputs("cannot init network\n", stderr);
		return 1;
	}

	std::puts("setup signal interruption");
	auto old = common::sigaction(SIGINT, +[](int) { running = false; });

	std::thread server{::server};

	{
		std::unique_lock lk{mRunning};
		cvRunning.wait(lk, []{ return static_cast<bool>(running); });
	}

	std::array<std::thread, 3> clients{std::thread{client, 0}, std::thread{client, 1}, std::thread{client, 2}};

	for(auto& thread: clients) thread.join();
	server.join();

	common::sigactionRestore(SIGINT, old);

	std::puts("terminate");

	net::release();
} catch(std::exception const& e) {
	std::fprintf(stderr, "#### [net::Exception] ####\n%s\n", e.what());
}
