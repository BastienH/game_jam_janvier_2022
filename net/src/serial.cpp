#include <array>
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>

#include <common/sigaction.hpp>
#include <net/server.hpp>
#include <net/serialise.hpp>
#include <net/deserialise.hpp>

namespace {

using Pos = std::tuple<float, float, float>;
using Info = std::tuple<bool, char, std::uint32_t>;

enum class Id: char { pos, info };

}

std::atomic_bool running = false;

std::mutex mRunning;
std::condition_variable cvRunning;

void server() {
	// Server
	std::printf("starting server... ");
	net::Server server{4273};

	std::printf("ready on %s:%d\n", server.ipAddr().c_str(), server.port());

	running = true;
	cvRunning.notify_one();

	server.accept(
		[](net::Client client) {
			std::printf("client connected from %s:%d\n", client.ipAddr().c_str(), client.port());

			auto data = client.recv();
			if(data.size()) {
				if(static_cast<Id>(data[0]) == Id::info) {
					auto info = net::deserialise<Info>(std::next(std::begin(data)), std::end(data));
					std::printf("[server] Info{%s, %c, %d}\n",
											std::get<0>(info)? "true":"false", std::get<1>(info), std::get<2>(info));
				} else if(static_cast<Id>(data[0]) == Id::pos) {
					auto pos = net::deserialise<Pos>(std::next(std::begin(data)), std::end(data));
					std::printf("[server] Pos{%.2f, %.2f, %.2f}\n",
											std::get<0>(pos), std::get<1>(pos), std::get<2>(pos));
				}
			}

			Pos pos{7.f, -3.f, 3.141592f};

			client.send(
				net::serialise(std::make_tuple(static_cast<char>(Id::pos))) +
				net::serialise(pos)
			);
		}
	);

	{
		std::unique_lock lk{mRunning};
		cvRunning.wait(lk, []{ return !static_cast<bool>(running); });
	}

	std::puts("disconnected");
}

void client() {
	std::printf("starting client... ");
	net::Client client{"localhost", 4273};

	std::printf("connected (%s:%d)\n", client.ipAddr().c_str(), client.port());

	Info info{true, 'A', 113};
	client.send(
		net::serialise(std::make_tuple(static_cast<char>(Id::info))) +
		net::serialise(info)
	);

	auto data = client.recv();
	if(data.size()) {
		if(static_cast<Id>(data[0]) == Id::info) {
			auto info = net::deserialise<Info>(std::next(std::begin(data)), std::end(data));
			std::printf("[client] Info{%s, %c, %d}\n",
									std::get<0>(info)? "true":"false", std::get<1>(info), std::get<2>(info));
		} else if(static_cast<Id>(data[0]) == Id::pos) {
			auto pos = net::deserialise<Pos>(std::next(std::begin(data)), std::end(data));
			std::printf("[client] Pos{%.2f, %.2f, %.2f}\n",
									std::get<0>(pos), std::get<1>(pos), std::get<2>(pos));
		}
	}
}

int main() try {
	if(!net::init()) {
		std::fputs("cannot init network\n", stderr);
		return 1;
	}

	std::puts("setup signal interruption");
	auto old = common::sigaction(SIGINT, +[](int) { running = false; cvRunning.notify_all(); });

	std::thread server{::server};

	{
		std::unique_lock lk{mRunning};
		cvRunning.wait(lk, []{ return static_cast<bool>(running); });
	}

	client();

	server.join();

	common::sigactionRestore(SIGINT, old);

	std::puts("terminate");

	net::release();
} catch(std::exception const& e) {
	std::fprintf(stderr, "#### [net::Exception] ####\n%s\n", e.what());
}
