#pragma once

#include <cstdint>
#include <tuple>

namespace net {

namespace impl {

template<typename, std::size_t, typename = void> struct TupleSizeAcc;
template<typename T, typename... Ts, std::size_t n>
struct TupleSizeAcc<std::tuple<T, Ts...>, n, std::enable_if_t<n>> {
	static constexpr std::size_t value = sizeof(T)+TupleSizeAcc<std::tuple<Ts...>, n-1>::value;
};
template<typename... Ts>
struct TupleSizeAcc<std::tuple<Ts...>, 0> {
	static constexpr std::size_t value = 0;
};

}

template<typename T, std::size_t n>
constexpr std::size_t tupleSizeAcc = impl::TupleSizeAcc<T, n>::value;

}
