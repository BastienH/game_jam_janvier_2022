#pragma once

#include <cstdint>
#include <functional>

#include "netfile.hpp"
#include "client.hpp"

namespace net {

class Server: public NetFile {
public:
	using ClientFn = std::function<void(Client)>;

public:
	explicit Server(std::uint16_t port, decltype(AF_UNSPEC) af = AF_UNSPEC, int backlog = 10) {
		setPort(port);

		bind(af);
		listen(backlog);
	}

	Server(Server const&) = delete;
	Server(Server&&) = default;

	void accept(ClientFn client, unsigned timeout = 500);

protected:
	void bind(decltype(AF_UNSPEC) af = AF_UNSPEC);
	void listen(int backlog = 10);

};

}
