#include "portability.hpp"

#ifdef WIN32
#pragma comment(lib, "Ws2_32.lib")
#endif

namespace net {

bool init() {
#ifdef WIN32
	WSADATA wsaData;
	auto iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);

	return iResult == 0;
#else
	return true;
#endif
}

void release() {
#ifdef WIN32
	WSACleanup();
#endif
}

int close(Socket fd) {
#ifdef WIN32
	return closesocket(fd);
#else
	return ::close(fd);
#endif
}

}
