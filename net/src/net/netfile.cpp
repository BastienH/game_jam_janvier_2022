#include <iterator>

#include <common/strerror.hpp>

#include "portability.hpp"
#include "exception.hpp"
#include "netfile.hpp"

namespace net {

NetFile::~NetFile() {
	if(_socket != invalidSocket) net::close(_socket);
}

void NetFile::send(std::string const& s, std::size_t n) {
	send(s.c_str(), n? n:s.size());
}

std::string NetFile::recvStr() {
	char buffer[BUFSIZ];
	auto len = recv(buffer, BUFSIZ-1);
	buffer[len] = 0;
	return buffer;
}

DataStream NetFile::recv() {
	char buffer[BUFSIZ];
	auto len = recv(buffer, BUFSIZ);
	return {std::begin(buffer), std::next(std::begin(buffer), len)};
}

void NetFile::recv(DataStream& data) {
	char buffer[BUFSIZ];
	auto len = recv(buffer, BUFSIZ);
	std::copy(std::begin(buffer), std::next(std::begin(buffer), len), std::back_inserter(data));
}

void NetFile::send(char const* buffer, std::size_t len) {
	if(!len) return;

	net::ssize_t ret;

	while(len && (ret = ::send(_socket, buffer, len, 0)) > 0) {
		buffer += ret;
		len -= ret;
	}

	if(ret == -1) throw Exception("send", common::strerror(errno));
	if(len != 0) throw std::runtime_error("should never happen");
}

void* NetFile::getInAddr(struct sockaddr* sa) {
	if(sa->sa_family == AF_INET)
		return &reinterpret_cast<struct sockaddr_in*>(sa)->sin_addr;
	return &reinterpret_cast<struct sockaddr_in6*>(sa)->sin6_addr;
}

std::string NetFile::strFromIPAddr(struct addrinfo* p) {
	char buffer[128];
	inet_ntop(p->ai_family, getInAddr(p->ai_addr), buffer, sizeof buffer);
	return buffer;
}

std::string NetFile::strFromIPAddr(struct sockaddr_storage* p) {
	char buffer[128];
	inet_ntop(p->ss_family, getInAddr(reinterpret_cast<struct sockaddr*>(p)), buffer, sizeof buffer);
	return buffer;
}

net::ssize_t NetFile::recv(char* buffer, std::size_t capacity) {
	net::ssize_t len;
	REQUIRE(len = ::recv(_socket, buffer, capacity, 0));
	return len;
}

}
