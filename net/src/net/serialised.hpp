#pragma once

#include <cstdint>
#include <stdexcept>
#include <tuple>
#include <utility>

#include "types.hpp"
#include "utility.hpp"

namespace net {

namespace impl {

template<std::size_t>
using ByteSpan = char;

template<typename> struct SerialisedRaw;

template<std::size_t... indices>
struct SerialisedRaw<std::index_sequence<indices...>> {
	using type = std::tuple<ByteSpan<indices>...>;

};

}

/*
 * Serialied::Raw: std::tuple<char...>
 * with as many char as required to store Ts...
 */
template<typename... Ts>
struct Serialised {
	using Raw = typename impl::SerialisedRaw<decltype(std::make_index_sequence<(sizeof(Ts) + ...)>())>::type;
	Raw raw;

	template<typename F>
	void apply(F f) const { apply(f, std::make_index_sequence<(sizeof(Ts) + ...)>()); }

private:
	template<typename F, std::size_t... indices>
	void apply(F f, std::index_sequence<indices...>) const {
		(f(std::get<indices>(raw)), ...);
	}
};

namespace impl {

template<typename> struct SerialisedTuple;
template<typename... Ts>
struct SerialisedTuple<std::tuple<Ts...>> {
	using type = net::Serialised<Ts...>;
};

}

template<typename T>
using SerialisedTuple = typename impl::SerialisedTuple<T>::type;

template<std::size_t n, typename S>
decltype(auto) get(S const& s) {
	return std::get<n>(s.raw);
}

namespace impl {

template<typename T0, std::size_t... indicesT0, typename T1, std::size_t... indicesT1>
net::Serialised<std::tuple_element_t<indicesT0, typename T0::Raw>..., std::tuple_element_t<indicesT1, typename T1::Raw>...>
serialiseMerge(T0 const& s0, std::index_sequence<indicesT0...>, T1 const& s1, std::index_sequence<indicesT1...>) {
	return {{get<indicesT0>(s0)..., get<indicesT1>(s1)...}};
}

template<typename T0, typename T1>
decltype(auto) serialiseMerge(T0 const& s0, T1 const& s1) {
	return serialiseMerge(s0, std::make_index_sequence<std::tuple_size_v<typename T0::Raw>>(),
												s1, std::make_index_sequence<std::tuple_size_v<typename T1::Raw>>());
}

}

template<typename... T0, typename... T1>
decltype(auto) operator+(Serialised<T0...> const& s0, Serialised<T1...> const& s1) {
	return impl::serialiseMerge(s0, s1);
}

namespace impl {

template<typename T, std::size_t... indices>
net::SerialisedTuple<T> prepareDataAsTuple(DataStream const& v, std::index_sequence<indices...>) {
	return {{v[indices]...}};
}

template<typename T, typename Iterator, std::size_t... indices>
net::SerialisedTuple<T> prepareDataAsTuple(Iterator begin, Iterator end, std::index_sequence<indices...>) {
	auto get = [&begin, &end](std::size_t i) {
		auto it = std::next(begin, i);
		if(it == end) throw std::out_of_range("prepareDataAsTuple: missing data");
		return *it;
	};
	return {{get(indices)...}};
}

}

template<typename T>
SerialisedTuple<T> prepareDataAsTuple(DataStream const& v) {
	return impl::prepareDataAsTuple<T>(v, std::make_index_sequence<tupleSizeAcc<T, std::tuple_size_v<T>>>());
}

template<typename T, typename Iterator>
SerialisedTuple<T> prepareDataAsTuple(Iterator begin, Iterator end) {
	return impl::prepareDataAsTuple<T>(begin, end, std::make_index_sequence<tupleSizeAcc<T, std::tuple_size_v<T>>>());
}

}
