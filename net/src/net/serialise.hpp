#pragma once

#include <tuple>

#include "converters.hpp"
#include "serialised.hpp"

namespace net {

inline Serialised<bool> serialise(bool v) { return {{+v}}; }
inline Serialised<char> serialise(char c) { return {{c}}; }
inline Serialised<std::uint8_t> serialise(std::uint8_t v) { return {{v}}; }
inline Serialised<std::uint16_t> serialise(std::uint16_t v) {
	auto bytes = conv::bytesFromInteger(v);
	return {{bytes[0], bytes[1]}};
}
inline Serialised<std::uint32_t> serialise(std::uint32_t v) {
	auto bytes = conv::bytesFromInteger(v);
	return {{bytes[0], bytes[1], bytes[2], bytes[3]}};
}
inline Serialised<float> serialise(float v) {
	return {serialise(conv::integerFromFloat<std::uint32_t>(v)).raw};
}

namespace impl {

template<typename T, std::size_t... indices>
decltype(auto) serialise(T const& data, std::index_sequence<indices...>) {
	return (net::serialise(std::get<indices>(data)) + ...);
}

}

template<typename... Ts>
decltype(auto) serialise(std::tuple<Ts...> const& data) {
	return impl::serialise(data, std::make_index_sequence<sizeof...(Ts)>());
}

}
