#include <cstring>
#include <numeric>
#include <vector>

#include <common/strerror.hpp>

#include "portability.hpp"
#include "client.hpp"
#include "exception.hpp"

namespace net {

Client::Client(std::string const& ipaddr, std::uint16_t port) {
	std::string actualAddr = ipaddr;

	setSocket(connectTo(actualAddr, port));
	setIpAddr(actualAddr);
	setPort(port);
}

Socket Client::connectTo(std::string& ipaddr, std::uint16_t port) {
	struct addrinfo hints;

	std::memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	char portstr[6] = "";
	std::snprintf(portstr, sizeof portstr, "%d", port); // check?

	struct addrinfo* result;
	if(int rv = getaddrinfo(ipaddr.c_str(), portstr, &hints, &result); rv != 0)
		throw Exception("getaddrinfo", gai_strerror(rv));

	Socket socket{invalidSocket};

	{
		std::vector<std::string> errors;

		struct addrinfo* p;
		for(p = result; p != nullptr; p = p->ai_next) {
			if((socket = ::socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
				errors.emplace_back("socket: "+common::strerror(errno));
				continue;
			}

			if(connect(socket, p->ai_addr, p->ai_addrlen) == -1) {
				errors.emplace_back("connect: "+common::strerror(errno));
				if(net::close(socket) == -1)
					errors.emplace_back("close: "+common::strerror(errno));
				continue;
			}

			break; // successful connection
		}

		if(p == nullptr) {
			freeaddrinfo(result);
			std::string trace = std::accumulate(std::begin(errors), std::end(errors), std::string{},
																					[](std::string const& base, std::string const& in) {
																						return base + "\n- "+in;
																					}
			);
			throw Exception("client failed to connect to "+ipaddr+":"+portstr, trace);
		}

		ipaddr = strFromIPAddr(p);

		freeaddrinfo(result);
	}

	return socket;
}

}
