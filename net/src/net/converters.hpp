#pragma once

#include <cstdint>
#include <utility>

#include "portability.hpp"

namespace net::conv {

inline std::uint16_t ntoh(std::uint16_t v) { return ntohs(v); }
inline std::uint16_t hton(std::uint16_t v) { return htons(v); }
inline std::uint32_t ntoh(std::uint32_t v) { return ntohl(v); }
inline std::uint32_t hton(std::uint32_t v) { return htonl(v); }

template<typename T, std::enable_if_t<(sizeof(T)>1)>* = nullptr>
char* bytesFromInteger(T& value) {
	value = hton(value);
	return reinterpret_cast<char*>(&value);
}

template<typename T, std::enable_if_t<(sizeof(T)>1)>* = nullptr>
T integerFromBytes(char* bytes) {
	return ntoh(*reinterpret_cast<T*>(bytes));
}

template<typename I, typename T, std::enable_if_t<sizeof(I) == sizeof(T)>* = nullptr>
I integerFromFloat(T const& value) {
	return reinterpret_cast<I const&>(value);
}

template<typename F, typename T, std::enable_if_t<sizeof(F) == sizeof(T)>* = nullptr>
F floatFromInteger(T const& value) {
	return reinterpret_cast<F const&>(value);
}

}
