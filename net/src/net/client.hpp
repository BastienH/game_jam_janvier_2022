#pragma once

#include "netfile.hpp"

namespace net {

class Client: public NetFile {
public:
	explicit Client(Socket fd, std::string const& ipAddr, std::uint16_t port):
		NetFile{fd}
	{
		setIpAddr(ipAddr);
		setPort(port);
	}

	explicit Client(std::string const& ipaddr, std::uint16_t port);

	Client(Client const&) = delete;
	Client(Client&&) = default;

private:
	static Socket connectTo(std::string& ipaddr, std::uint16_t port);
};

}
