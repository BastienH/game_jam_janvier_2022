#include <cstring>
#include <numeric>
#include <thread>

#include <common/poll.hpp>
#include <common/strerror.hpp>

#include "portability.hpp"
#include "exception.hpp"
#include "server.hpp"

namespace net {

void Server::bind(decltype(AF_UNSPEC) af) {
	struct addrinfo hints;

	std::memset(&hints, 0, sizeof hints);
	hints.ai_family = af;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = 0;

	char port[6] = "";
	std::snprintf(port, sizeof port, "%d", Server::port()); // check?

	struct addrinfo* result;
	if(int rv = getaddrinfo(nullptr, port, &hints, &result); rv != 0)
		throw Exception("getaddrinfo", gai_strerror(rv));

	{
		Socket socket;
		std::vector<std::string> errors;

		struct addrinfo* p;
		for(p = result; p != nullptr; p = p->ai_next) {
			if((socket = ::socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
				errors.emplace_back("socket: "+common::strerror(errno));
				continue;
			}

			setSocket(socket);

			{
				int opt = 1;
#ifdef WIN32
				int ret = setsockopt(Server::socket(), SOL_SOCKET, SO_REUSEADDR, reinterpret_cast<char const*>(&opt), sizeof opt);
#else
				int ret = setsockopt(Server::socket(), SOL_SOCKET, SO_REUSEADDR, &opt, sizeof opt);
#endif
				if(ret == -1) {
					errors.emplace_back("setsockopt: "+common::strerror(errno));
					continue;
				}
			}

			if(::bind(Server::socket(), p->ai_addr, p->ai_addrlen) == -1) {
				std::string addr = strFromIPAddr(p);

				errors.emplace_back("bind("+addr+"): "+common::strerror(errno));
				if(net::close(Server::socket()) == -1)
					errors.emplace_back("close: "+common::strerror(errno));
				continue;
			}

			break; // successful binding
		}

		if(p == nullptr) {
			freeaddrinfo(result);
			std::string trace = std::accumulate(std::begin(errors), std::end(errors), std::string{},
																					[](std::string const& base, std::string const& in) {
																						return base + "\n- "+in;
																					}
			);
			throw Exception("cannot setup server", trace);
		}

		setIpAddr(strFromIPAddr(p));

		freeaddrinfo(result);
	}
}

void Server::listen(int backlog) {
	REQUIRE(::listen(Server::socket(), backlog));
}

void Server::accept(ClientFn client, unsigned timeout) {
	struct sockaddr_storage addr;
	socklen_t len = sizeof addr;
	Socket socket;
	int r;

	{
		struct pollfd pfd;
		pfd.fd = Server::socket();
		pfd.events = POLLIN;

		REQUIRE(r = common::poll(&pfd, 1, timeout));
	}

	if(r) {
		REQUIRE(socket = ::accept(Server::socket(), reinterpret_cast<struct sockaddr*>(&addr), &len));

		std::string ipAddr = strFromIPAddr(&addr);

		std::thread{client, Client(socket, ipAddr, 0)}.detach();
	}
}

}
