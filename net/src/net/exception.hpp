#pragma once

#include <stdexcept>
#include <cstring>

#include <common/strerror.hpp>

#define REQUIRE(call) if((call) == -1) throw Exception(#call, common::strerror(errno))

namespace net {

class Exception: public std::runtime_error {
public:
	explicit Exception(std::string const& m):
		std::runtime_error(m)
	{}

	explicit Exception(std::string const& m, std::string const& e):
		Exception(m+": "+e)
	{}
};

}
