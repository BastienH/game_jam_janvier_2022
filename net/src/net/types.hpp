#pragma once

#include <deque>

namespace net {

using DataStream = std::deque<char>;

inline void consume(DataStream& data, std::size_t n) {
	while(n --> 0) data.pop_front();
}

}
