#pragma once

extern "C" {

#include <fcntl.h>
#include <sys/types.h>

#ifdef WIN32

#include <WinSock2.h>
#include <WS2tcpip.h>
#pragma warning(disable:4267)

#else

#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <unistd.h>

#endif

}

namespace net {

#ifdef WIN32

using ssize_t = int;
using Socket = SOCKET;
constexpr Socket invalidSocket = INVALID_SOCKET;

#else

using ssize_t = ::ssize_t;
using Socket = int;
constexpr Socket invalidSocket = -1;

#endif

bool init();
void release();
int close(Socket fd);

}
