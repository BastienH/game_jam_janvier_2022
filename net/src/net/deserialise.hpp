#pragma once

#include <tuple>

#include "converters.hpp"
#include "serialised.hpp"
#include "types.hpp"
#include "utility.hpp"

namespace net {

template<typename, std::size_t> struct Deserialiser;

template<std::size_t offset>
struct Deserialiser<bool, offset> {
	template<typename Serial>
	static bool get(Serial const& s) { return !!net::get<offset>(s); }
};

template<std::size_t offset>
struct Deserialiser<char, offset> {
	template<typename Serial>
	static char get(Serial const& s) { return net::get<offset>(s); }
};

template<std::size_t offset>
struct Deserialiser<std::uint8_t, offset> {
	template<typename Serial>
	static std::uint8_t get(Serial const& s) { return net::get<offset>(s); }
};

template<std::size_t offset>
struct Deserialiser<std::uint16_t, offset> {
	template<typename Serial>
	static std::uint16_t get(Serial const& s) {
		char bytes[]{net::get<offset+0>(s), net::get<offset+1>(s)};
		return conv::integerFromBytes<std::uint16_t>(bytes);
	}
};

template<std::size_t offset>
struct Deserialiser<std::uint32_t, offset> {
	template<typename Serial>
	static std::uint32_t get(Serial const& s) {
		char bytes[]{net::get<offset+0>(s), net::get<offset+1>(s), net::get<offset+2>(s), net::get<offset+3>(s)};
		return conv::integerFromBytes<std::uint32_t>(bytes);
	}
};

template<std::size_t offset>
struct Deserialiser<float, offset> {
	template<typename Serial>
	static float get(Serial const& s) {
		auto i = Deserialiser<std::uint32_t, offset>::get(s);
		return conv::floatFromInteger<float>(i);
	}
};

namespace impl {

template<typename T> struct IsTuple: std::false_type {};
template<typename... Ts> struct IsTuple<std::tuple<Ts...>>: std::true_type {};
template<typename T> constexpr bool isTuple = IsTuple<T>::value;

}

template<typename T, std::enable_if_t<!impl::isTuple<T>>* = nullptr>
T deserialise(Serialised<T> const& s) {
	return Deserialiser<T, 0>::get(s);
}

namespace impl {

template<typename, std::size_t, std::size_t> struct DeserialiserTuple;

template<typename... Ts, std::size_t i, std::size_t offset>
struct DeserialiserTuple<std::tuple<Ts...>, i, offset> {
	using Tuple = std::tuple<Ts...>;
	using Return = std::tuple_element_t<i, Tuple>;
	using Serial = SerialisedTuple<Tuple>;

	static Return get(Serial const& s) {
		return net::Deserialiser<Return, offset>::get(s);
	}
};

}

namespace impl {

template<typename T, std::size_t... indices>
T deserialiseTuple(net::SerialisedTuple<T> const& s, std::index_sequence<indices...>) {
	return {
		net::Deserialiser<
			std::tuple_element_t<indices, T>,
			net::tupleSizeAcc<T, indices>
		>::get(s)...
	};
}

}

template<typename T, std::enable_if_t<impl::isTuple<T>>* = nullptr>
T deserialise(SerialisedTuple<T> const& s) {
	return impl::deserialiseTuple<T>(s, std::make_index_sequence<std::tuple_size_v<T>>());
}

template<typename T, std::enable_if_t<impl::isTuple<T>>* = nullptr>
T deserialise(DataStream const& s) {
	return deserialise<T>(prepareDataAsTuple<T>(s));
}

template<typename T, typename Iterator, std::enable_if_t<impl::isTuple<T>>* = nullptr>
T deserialise(Iterator begin, Iterator end) {
	return deserialise<T>(prepareDataAsTuple<T>(begin, end));
}

}
