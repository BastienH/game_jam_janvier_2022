#pragma once

#include <string>
#include <utility>

#include "portability.hpp"
#include "serialise.hpp"
#include "types.hpp"

namespace net {

class NetFile {
	Socket _socket;
	std::string _ipAddr;
	std::uint16_t _port;

public:
	explicit NetFile(): _socket{invalidSocket} {}
	explicit NetFile(Socket socket):
		_socket{socket}
	{}

	NetFile(NetFile const&) = delete;
	NetFile(NetFile&& o):
		_socket{std::exchange(o._socket, invalidSocket)},
		_ipAddr{std::exchange(o._ipAddr, "")},
		_port{std::exchange(o._port, 0)}
	{}

	NetFile& operator=(NetFile const&) = delete;

	NetFile& operator=(NetFile&& o) {
		std::swap(_socket, o._socket);
		std::swap(_ipAddr, o._ipAddr);
		std::swap(_port, o._port);
		return *this;
	}

	~NetFile();

	void send(std::string const&, std::size_t = 0);
	[[nodiscard]] std::string recvStr();

	[[nodiscard]] DataStream recv();
	void recv(DataStream&);
	void send(char const*, std::size_t);

	template<typename... Ts>
	void send(Serialised<Ts...> const& s) { send(s.raw); }

	template<typename... Ts, std::enable_if_t<(std::is_same_v<Ts, char> && ...)>* = nullptr>
	void send(std::tuple<Ts...> const& t) {
		send(t, std::make_index_sequence<sizeof...(Ts)>());
	}

private:
	template<typename T, std::size_t... indices>
	void send(T const& t, std::index_sequence<indices...>) {
		char buffer[sizeof...(indices)]{std::get<indices>(t)...};
		send(buffer, sizeof...(indices));
	}

protected:
	[[nodiscard]] static void* getInAddr(struct sockaddr*);
	[[nodiscard]] static std::string strFromIPAddr(struct addrinfo*);
	[[nodiscard]] static std::string strFromIPAddr(struct sockaddr_storage* p);

	void setSocket(Socket socket) { _socket = socket; }
	Socket const& socket() const { return _socket; }

	void setIpAddr(std::string const& ipAddr) { _ipAddr = ipAddr; }
	void setPort(std::uint16_t port) { _port = port; }

public:
	std::string const& ipAddr() const { return _ipAddr; }
	std::uint16_t port() const { return _port; }

private:
	[[nodiscard]] net::ssize_t recv(char*, std::size_t);

};

}
