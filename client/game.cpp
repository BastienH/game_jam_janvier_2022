#include "game.hpp"

#include <SFML/Window/Event.hpp>
#include <SFML/Window/VideoMode.hpp>

#include <chrono>
#include <vector>

#include "systems/renderer.hpp"
#include "systems/player.hpp"
#include "systems/projectile.hpp"

#include "api.hpp"

namespace client::game
{
std::unique_ptr<sf::RenderWindow> window{ nullptr };
utils::map::Map map{};
std::vector<std::unique_ptr<systems::System>> systems{};

void init()
{
	map = utils::map::loader::load("resources/maps/1.json");
	window = std::make_unique<sf::RenderWindow>(sf::VideoMode(1600, 900), "???");
	getWindow().setFramerateLimit(60);

	api::init();
}

void run()
{
	using namespace std::literals::chrono_literals;

	auto deltaTime = 0ms;

	systems.push_back(std::make_unique<systems::Player>(Point{ 250, 50 }, sf::Color::Red));
	systems.push_back(std::make_unique<systems::ProjectileHandler>());

	systems.push_back(std::make_unique<systems::Renderer>());

	while (window->isOpen())
	{
		auto start = std::chrono::high_resolution_clock::now();

		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
	            window->close();
				
				return;
			}
		}

		window->clear(sf::Color::White);

		for(auto i = 0u; i < systems.size(); ++i)
		{
			systems[i]->update(deltaTime);
		}

		api::send();

		window->display();

		auto end  = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	}
}

sf::RenderWindow& getWindow() { return *window; }
utils::map::Map& getMap(){return map;}


} // namespace core::Game
