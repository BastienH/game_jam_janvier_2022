#include "game.hpp"

using namespace client;

int main(int, const char**)
{
    game::init();

    game::run();    

    return 0;
}