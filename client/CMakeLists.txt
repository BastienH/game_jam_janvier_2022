
add_executable(client main.cpp)
target_link_libraries(client PRIVATE CONAN_PKG::sfml CONAN_PKG::box2d utils net protocol)
target_include_directories(client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

SET(_hdr
    "game.hpp"
    "api.hpp"
    "systems/renderer.hpp"
    "systems/player.hpp"
    "systems/projectile.hpp"
)

SET(_src
    "game.cpp"
    "api.cpp"
    "systems/renderer.cpp"
    "systems/player.cpp"
    "systems/projectile.cpp"
)

list(TRANSFORM _hdr PREPEND "${CMAKE_CURRENT_SOURCE_DIR}/")
list(TRANSFORM _src PREPEND "${CMAKE_CURRENT_SOURCE_DIR}/")

target_sources(client PUBLIC ${_hdr})
target_sources(client PRIVATE ${_src})

add_custom_target(run_client
    COMMAND cd ${CMAKE_BINARY_DIR}/bin && ./client
    COMMAND cd ${CMAKE_BINARY_DIR}
)
add_dependencies(run_client client)