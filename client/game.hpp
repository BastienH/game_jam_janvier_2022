#pragma once

#include <SFML/Graphics.hpp>
#include <map/loader.hpp>

#include "systems/player.hpp"

namespace client
{
    namespace game
    {
        sf::RenderWindow& getWindow();
        utils::map::Map& getMap();

        void init();

        void run();
    }
}