#include "projectile.hpp"
#include "renderer.hpp"
#include <SFML/Graphics.hpp>

namespace client::systems
{
    //Projectile
    void Projectile::update(const Point& d)
    {
        pos.x = d.x;
        pos.y = d.y;
    }

    void Projectile::draw(sf::RenderTarget& rt, sf::RenderStates rs) const
    {
        sf::CircleShape c{ proj_radius };
        c.setOrigin(c.getRadius(), c.getRadius());
        c.setPosition(pos.x, pos.y);
        c.setFillColor(sf::Color::Red);
        rt.draw(c, rs);
    }

    //ProjectileHandler
    ProjectileHandler::ProjectileHandler()
    {
        Renderer::addElementToDraw(this);
    }
    
    void ProjectileHandler::update(const std::chrono::milliseconds& dt)
    {
        for (auto& [id, projectile] : _projectiles)
        {
            Point speed{ proj_speed * projectile.direction.x, proj_speed * projectile.direction.y };

            speed.x *= dt.count();
            speed.y *= dt.count();

            float hypot{ std::hypot(speed.x, speed.y) };

            if (hypot > 0)
            {
                Point newPos{ projectile.pos.x + speed.x * abs(speed.x) / hypot, projectile.pos.y + speed.y * abs(speed.y) / hypot };
                updateProjectileById(id, newPos);
            }
        }
    }

    void ProjectileHandler::draw(sf::RenderTarget& rt, sf::RenderStates rs) const
    {
        for (auto& [id, projectile] : _projectiles)
        {
            rt.draw(projectile, rs);
        }
    }

    std::unordered_map<unsigned short, Projectile> ProjectileHandler::_projectiles{};

    Projectile& ProjectileHandler::newProjectile(const sf::Color& color, const Point& position, const Point& direction)
    {
        static unsigned id{ 0 };

        _projectiles[id].pos = position;
        _projectiles[id].direction = direction;
        _projectiles[id].color = color;

        return _projectiles[id++];
    }

    void ProjectileHandler::removeProjectileById(unsigned short id)
    {
        _projectiles.erase(id);
    }

    std::optional< std::reference_wrapper<Projectile>> ProjectileHandler::getProjectileById(unsigned short id)
    {
        if (_projectiles.find(id) == _projectiles.end())
        {
            return std::nullopt;
        }
        return _projectiles[id];
    }

    void ProjectileHandler::updateProjectileById(unsigned short id, const Point& pos)
    {
        auto proj = getProjectileById(id);

        if (proj.has_value())
        {
            proj.value().get().pos = pos;
        }
    }
}
