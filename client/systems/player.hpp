#pragma once

#include <chrono>

#include "system.hpp"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <map/point.hpp>

constexpr Point view_size{ 1600, 900 };
constexpr float view_speed{ 2 };
constexpr std::chrono::milliseconds player_timer_max{ 200 };


namespace client::systems
{
    struct Player : System, sf::Drawable
    {
        Player(const Point& spawn, const sf::Color& color);

        void update(const std::chrono::milliseconds& dt) override;

        void draw(sf::RenderTarget& rt, sf::RenderStates rs) const override;

        void updatePosition(const Point& pos);

        private:
            sf::View _view{ sf::FloatRect(0, 0, view_size.x, view_size.y) };

            Point _pos{ 250, 50 };
            sf::Color _color{ sf::Color::Red };
            float _angle{ 0 };

            std::chrono::milliseconds _timer{ 0 }, _timerMax{ player_timer_max };
    };

    namespace players
    {
       extern std::vector<Player> list; 
			 extern std::uint8_t pId;
    } 
}
