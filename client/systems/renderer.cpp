#include "renderer.hpp"
#include "game.hpp"
#include <SFML/Graphics.hpp>

namespace client::systems
{
    void Renderer::update(const std::chrono::milliseconds&)
    {
        utils::map::Map map = game::getMap();

        for(const auto& wall : map.walls)
        {
            unsigned i{0};
            sf::ConvexShape convex;

            convex.setFillColor(sf::Color::White);
            convex.setOutlineThickness(2);
            convex.setOutlineColor(sf::Color::Black);

            convex.setPointCount(wall.points.size());
            for(const auto& point: wall.points)
            {
                convex.setPoint(i++, sf::Vector2f(point.x, point.y));
            }
            game::getWindow().draw(convex);
        }

        for (const auto& element : _drawables)
        {
            game::getWindow().draw(*element);
        }
    }

    //Static

    std::vector<sf::Drawable*> Renderer::_drawables{};

    void Renderer::addElementToDraw(sf::Drawable* element)
    {
        _drawables.push_back(element);
    }

    void Renderer::removeElementToDraw(sf::Drawable* element)
    {
        _drawables.erase(std::remove(_drawables.begin(), _drawables.end(), element), _drawables.end());
    }

}
