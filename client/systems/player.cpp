#include "player.hpp"

#include "game.hpp"
#include <SFML/Graphics.hpp>

#include "projectile.hpp"
#include "renderer.hpp"

namespace client::systems
{
    Player::Player(const Point& spawn, const sf::Color& color)
        :_pos(spawn), _color(color)
    {
        _view.setCenter(sf::Vector2f(_pos.x, _pos.y));
        game::getWindow().setView(_view);

        Renderer::addElementToDraw(this);
    }

    void Player::update(const std::chrono::milliseconds& dt)
    {
        auto const& center = sf::Vector2f{800, 450};

        auto const & mousePos = sf::Vector2f(sf::Mouse::getPosition(game::getWindow()));
        auto const & dp = game::getWindow().mapPixelToCoords(sf::Vector2i(mousePos.x, mousePos.y));
        _angle = std::atan2(dp.y - _pos.y, dp.x - _pos.x) / 3.14f * 180;

        //Handle view

        bool up{ sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Z) };
        bool down{ sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S) };
        bool left{ sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q) };
        bool right{ sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D) };

        Point speed{ (right - left) * view_speed, (down - up) * view_speed };

        speed.x *= dt.count();
        speed.y *= dt.count();

        float hypot{ std::hypot(speed.x, speed.y) };

        if (hypot > 0)
        {
            Point newPos{ _pos.x + speed.x * abs(speed.x) / hypot, _pos.y + speed.y * abs(speed.y) / hypot };
            updatePosition(newPos);
        }

        if (_timer.count() > 0)
        {
            _timer -= dt;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && _timer.count() <= 0)
        {
            _timer = _timerMax;
            
            ProjectileHandler::newProjectile(_color, _pos, { static_cast<float>(cos(_angle*3.14/180)), static_cast<float>(sin(_angle * 3.14 / 180)) });
        }
    }

    void Player::draw(sf::RenderTarget& rt, sf::RenderStates rs) const
    {
        sf::RectangleShape noze{ {40, 5} };
        noze.setFillColor(sf::Color::Blue);
        noze.setRotation(_angle);
        noze.setPosition(_pos.x, _pos.y);

        sf::CircleShape c{ 70 };
        c.setOrigin(c.getRadius(), c.getRadius());
        c.setPosition(_pos.x, _pos.y);
        c.setFillColor(_color);

        game::getWindow().setView(_view);
        game::getWindow().draw(c);
        game::getWindow().draw(noze);
    }

    void Player::updatePosition(const Point& pos)
    {
        _pos = pos;
        _view.setCenter(sf::Vector2f(_pos.x, _pos.y));
    }

    namespace players
    {
       std::vector<Player> list{}; 
			 std::uint8_t pId{};
    } 
}
