#pragma once

#include <chrono>

namespace client::systems
{
    struct System
    {
        virtual void update(const std::chrono::milliseconds& dt) = 0;
    };
}
