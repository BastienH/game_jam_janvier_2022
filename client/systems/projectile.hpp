#pragma once

#include "system.hpp"

#include <SFML/Graphics.hpp>

#include <unordered_map>
#include <map/point.hpp>
#include <optional>

namespace client::systems
{
    constexpr float proj_speed{ 1 };
    constexpr float proj_radius{ 5 };

    struct Projectile : sf::Drawable
    {
        Point pos{ 0, 0 };
        Point direction{ 0, 0 };
        sf::Color color;

        void update(const Point& d);

        void draw(sf::RenderTarget& rt, sf::RenderStates rs) const override;
    };

    struct ProjectileHandler : System, sf::Drawable
    {
        ProjectileHandler();

        void update(const std::chrono::milliseconds& dt) override;

        void draw(sf::RenderTarget& rt, sf::RenderStates rs) const override;


        static Projectile& newProjectile(const sf::Color& color, const Point& position, const Point& direction);
        static void removeProjectileById(unsigned short id);
        static std::optional< std::reference_wrapper<Projectile>> getProjectileById(unsigned short id);
        static void updateProjectileById(unsigned short id, const Point& pos);

        private:
            static std::unordered_map<unsigned short, Projectile> _projectiles;
    };
}
