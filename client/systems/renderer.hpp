#pragma once

#include "system.hpp"
#include <SFML/Graphics.hpp>

namespace client::systems
{
    struct Renderer : System
    {
        void update(const std::chrono::milliseconds& dt) override;

        static void addElementToDraw(sf::Drawable* element);
        static void removeElementToDraw(sf::Drawable* element);

        private:
            static std::vector<sf::Drawable*> _drawables;
    };
}
