#pragma once

#include <string>

namespace client::api
{
	void init(std::string const& ipaddr = "localhost", std::uint16_t port = 4273);

	void send();

	void recv();
}