#include "api.hpp"

#include "game.hpp"

#include <net/client.hpp>
#include <net/deserialise.hpp>
#include <protocol/protocol.hpp>

#include <SFML/Graphics.hpp>

#include <memory>

using namespace protocol;

namespace message
{
	using player = std::tuple<uint16_t, float, float, float, float, uint16_t>;
	using proj = std::tuple<uint16_t, float, float, uint16_t>;
	using playerInit = std::tuple<uint16_t>;
	using input = std::tuple<uint16_t, bool, bool, bool, bool, bool, float>;
}

namespace client::api
{
	using MyProt =
	Messages<
		Message<0, message::player>,
		Message<1, message::proj>,
		Message<2, message::playerInit>,
		Message<3, message::input>
	>;

	struct Handler {
		template<typename T>
		static void received(T const& t) { received(t); }
	};

	std::unique_ptr<net::Client> client{ nullptr };

	void init(std::string const& ipaddr, std::uint16_t port)
	{
		if (!net::init()) {
			std::fputs("cannot init network\n", stderr);
			return;
		}

		client = std::make_unique<net::Client>(ipaddr, port);

		net::DataStream data;
		while(data.size() < sizeof(std::uint16_t))
			client->recv(data);

		std::tie(systems::players::pId) = net::deserialise<std::tuple<std::uint16_t>>(data);
	}

	void send()
	{
		bool up{ sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Z) };
		bool down{ sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S) };
		bool left{ sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q) };
		bool right{ sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D) };
		bool leftMouse{ sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) };
		auto const & mousePos = sf::Vector2f(sf::Mouse::getPosition(game::getWindow()));
        auto const & dp = game::getWindow().mapPixelToCoords(sf::Vector2i(mousePos.x, mousePos.y));
        auto angle = std::atan2(dp.y - game::getWindow().getView().getCenter().y, dp.x - game::getWindow().getView().getCenter().x) / 3.14f * 180;

		auto inputs = std::make_tuple(std::uint16_t(systems::players::pId), up, down, left, right, leftMouse, angle);
		protocol::sendMessage<MyProt>(*client, 3, inputs);
	}

	void recv()
	{

	}
}
