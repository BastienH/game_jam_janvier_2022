#include "sigaction.hpp"

namespace common {

#ifdef WIN32

#else

struct sigaction sigaction(int sig, void(*handler)(int)) {
	struct sigaction act, oldact;
	sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
	act.sa_handler = handler;
	::sigaction(sig, &act, &oldact);
	return oldact;
}

#endif

}
