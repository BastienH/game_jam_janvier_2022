#include "poll.hpp"

namespace common {

int poll(struct pollfd* fds, unsigned nfds, unsigned timeout) {
#ifdef WIN32
	return WSAPoll(fds, nfds, timeout);
#else
	return ::poll(fds, nfds, timeout);
#endif
}

}
