#pragma once

extern "C" {

#include <signal.h>

}

namespace common {

#ifdef WIN32

inline bool sigaction(int sig, void(*handler)(int)) {
	signal(sig, handler);
	return true;
}

inline void sigactionRestore(int, bool) {}

#else

[[nodiscard]] struct sigaction sigaction(int sig, void(*handler)(int));

inline void sigactionRestore(int sig, struct sigaction& oldact) {
	::sigaction(sig, &oldact, nullptr);
}

#endif

}
