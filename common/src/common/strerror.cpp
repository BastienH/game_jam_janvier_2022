#include <cstring>

#include "strerror.hpp"

namespace common {

#ifdef WIN32

std::string strerror(int errnum) {
	char buffer[BUFSIZ];
	if(strerror_s(buffer, sizeof buffer, errnum) == 0)
		return buffer;
	return "{{strerror_s failure}}";
}

#else

std::string strerror(int errnum) {
	char buffer[BUFSIZ]; // GNU version of strerror_r
	return strerror_r(errnum, buffer, sizeof buffer);
}

#endif

}
