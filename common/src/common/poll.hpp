#pragma once

extern "C" {

#ifdef WIN32

#include <WinSock2.h>

#else

#include <poll.h>

#endif

}

namespace common {

int poll(struct pollfd* fds, unsigned nfds, unsigned timeout);

}
