#pragma once

#include <string>

namespace common {

std::string strerror(int);

}
