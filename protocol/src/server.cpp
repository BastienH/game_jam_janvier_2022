#include <common/sigaction.hpp>
#include <net/server.hpp>
#include <protocol/protocol.hpp>

#include <thread>

#include "common.hpp"

std::atomic_bool running = false;

int main(int argc, char** argv) {
	if(!net::init()) {
		std::fputs("cannot init network\n", stderr);
		return 1;
	}

	constexpr char const* defaultAddress = "localhost";

	auto old = common::sigaction(SIGINT, +[](int) { running = false; });

	net::Server server{4273, AF_INET};
	std::printf("connected to %s:%u\n", server.ipAddr().c_str(), server.port());

	running = true;
	while(running) {
		server.accept(
			[](net::Client client) {
				protocol::sendMessage<msg::Prot>(client, 757935405, std::make_tuple('O','k','!'));
			}
		);
	}

	common::sigactionRestore(SIGINT, old);

	net::release();
}
