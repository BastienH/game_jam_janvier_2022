#pragma once

#include <atomic>
#include <cstdint>
#include <cstdio>
#include <thread>
#include <variant>

#include <net/netfile.hpp>
#include <net/deserialise.hpp>

namespace protocol {

using Id = std::uint32_t;

template<Id id, typename T>
struct Message {
	template<typename U>
	static bool validType() { return std::is_same_v<T, U>; }

	static std::size_t size() {
		return net::tupleSizeAcc<T, std::tuple_size_v<T>>;
	}

	template<typename Handler>
	static void parse(net::DataStream& data) {
		Handler::received(net::deserialise<T>(data));
		net::consume(data, size());
	}
};

template<typename...> struct Messages;

template<Id... ids, typename... Ts>
struct Messages<Message<ids, Ts>...> {
};

template<typename T> struct Dispatch;
template<Id... ids, typename... Ts>
struct Dispatch<Messages<Message<ids, Ts>...>> {
	static bool valid(Id id) {
		bool ok = false;
		(((id == ids) && (ok = true)), ...);
		return ok;
	}

	template<typename T>
	static bool validType(Id id) {
		bool ok = false;
		(((id == ids) && (ok = Message<ids, Ts>::template validType<T>())), ...);
		return ok;
	}

	static auto size(Id id) {
		return ((id == ids? Message<ids, Ts>::size():0) + ...);
	}

	template<typename Handler>
	static void parse(net::DataStream& data, Id id) {
		bool ok = false;
		(((id == ids) && (Message<ids, Ts>::template parse<Handler>(data), ok = true)), ...);
		if(!ok) std::fprintf(stderr, "[protocol] no message associated to id %u\n", id);
	}
};

template<typename Handler, typename M>
[[nodiscard]] auto receiveMessage(net::NetFile& n) {
	using D = Dispatch<M>;

	net::DataStream data;

	while(data.size() < sizeof(Id)) n.recv(data);

	Id id;
	std::tie(id) = net::deserialise<std::tuple<Id>>(data);
	net::consume(data, sizeof id);

	if(D::valid(id)) {
		auto size = D::size(id);

		while(data.size() < size) n.recv(data);
		D::template parse<Handler>(data, id);
	} else {
		std::fprintf(stderr, "[recv] ignore invalid id (%u)\n", id);
	}

	return data;
}

template<typename Handler, typename M>
void receiveMessageLoop(net::NetFile& n, std::atomic_bool const& running) {
	std::thread{[&n, &running]{
		using D = Dispatch<M>;

		net::DataStream data;
		Id id;

		while(running) try {
			while(running && data.size() < sizeof id) n.recv(data);

			std::tie(id) = net::deserialise<std::tuple<Id>>(data);

			if(D::valid(id)) {
				net::consume(data, sizeof id);
				auto size = D::size(id);

				while(running && data.size() < size) n.recv(data);
				D::template parse<Handler>(data, id);
			} else {
				net::consume(data, 1); // do not consume sizeof id to help syncing
				std::fprintf(stderr, "[recv] ignore invalid id (%u)\n", id);
			}
		} catch(std::exception const& e) {
			std::fprintf(stderr, "[recv] %s\n", e.what());
		}
	}}.detach();
}

template<typename M, typename T>
void sendMessage(net::NetFile& n, Id id, T const& t) {
	using D = Dispatch<M>;

	if(D::valid(id)) {
		if(D::template validType<T>(id)) {
			n.send(net::serialise(std::make_tuple(id)));
			n.send(net::serialise(t));
		} else {
			std::fprintf(stderr, "[send] invalid type for id (%u)\n", id);
		}
	} else {
		std::fprintf(stderr, "[send] invalid id (%u)\n", id);
	}
}

}
