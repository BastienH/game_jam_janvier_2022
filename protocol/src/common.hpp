#pragma once

#include <protocol/protocol.hpp>

namespace msg {

using namespace protocol;

using Char = std::tuple<char>;
using U32Char = std::tuple<std::uint32_t, char>;
using FloatChar = std::tuple<float, char>;
using Char3 = std::tuple<char, char, char>;
using U32CharU16 = std::tuple<std::uint32_t, char, std::uint16_t>;

using Prot =
Messages<
	Message<0, Char>,
	Message<1, U32Char>,
	Message<724249387, FloatChar>,
	Message<757935405, Char3>,
	Message<168430090, U32CharU16>
>;

}
