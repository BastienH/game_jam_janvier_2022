#include <common/sigaction.hpp>
#include <net/client.hpp>
#include <protocol/protocol.hpp>

#include <thread>

#include "common.hpp"

inline void received(std::tuple<char> const& t) { std::printf("<%c>\n", std::get<0>(t)); }
inline void received(std::tuple<std::uint32_t, char> const& t) {
	std::printf("<%u, %c>\n", std::get<0>(t), std::get<1>(t));
}
void received(std::tuple<float, char> const& t) {
	std::printf("<%f, %c>\n", std::get<0>(t), std::get<1>(t));
}
void received(std::tuple<char, char, char> const& t) {
	std::printf("<%c, %c, %c>\n", std::get<0>(t), std::get<1>(t), std::get<2>(t));
}
void received(std::tuple<std::uint32_t, char, std::uint16_t> const& t) {
	std::printf("<%u, %c, %u>\n", std::get<0>(t), std::get<1>(t), std::get<2>(t));
}

struct Handler {
	template<typename T>
	static void received(T const& t) { ::received(t); }
};

std::atomic_bool running = true;

int main(int argc, char** argv) {
	if(!net::init()) {
		std::fputs("cannot init network\n", stderr);
		return 1;
	}

	constexpr char const* defaultAddress = "localhost";

	auto old = common::sigaction(SIGINT, +[](int) { running = false; });

	net::Client client{argc==2? argv[1]:defaultAddress, 4273};
	std::printf("connected to %s:%u\n", client.ipAddr().c_str(), client.port());

	protocol::receiveMessageLoop<Handler, msg::Prot>(client, running);
	std::puts("reception setup");

	// do anything else
	while(running) std::this_thread::sleep_for(std::chrono::milliseconds(400));

	common::sigactionRestore(SIGINT, old);

	net::release();
}
