#include "api.hpp"
#include <map/loader.hpp>
#include "player.hpp"
#include "projectile.hpp"

#include "settings.hpp"
Settings settings;

#include <thread>

using namespace server;

int main()
{
    b2Vec2 gravity;
    gravity.Set(0.0f, 0.0f);
    auto world = new b2World(gravity);

	auto map = utils::map::loader::load("resources/maps/1.json");

	for (auto const & wall : map.walls) {
		b2BodyDef bodyDef;
		bodyDef.type = b2_staticBody;
		bodyDef.position = b2Vec2(0, -0);
		auto * body = world->CreateBody(&bodyDef);

		b2FixtureDef fd;
		fd.shape = &wall.polygon;

		body->CreateFixture(&fd);
	}
	std::vector<Player> players;

	std::thread read([&players, world](){
		using namespace std::chrono_literals;
		static std::chrono::steady_clock::time_point t_begin = std::chrono::steady_clock::now();
		std::chrono::duration<double> frameTime(0);
		float frameTimef = 0;

		while (true) {
			t_begin = std::chrono::steady_clock::now();
			//std::this_thread::sleep_for(500ms);

			if (server::api::p_datas.size() > players.size()) {
				players.emplace_back(b2Vec2{500, 0}, world);
			}

			for (auto & pData : server::api::p_datas) {
				const auto & pId = pData.id;
				players[pId].move(b2Vec2{static_cast<float>(pData.right - pData.left), static_cast<float>(pData.up - pData.down)});

				if (players[pId].cd > 0)
				{
					players[pId].cd -= frameTimef;
				}

				if (pData.click && players[pId].cd <= 0)
				{
					players[pId].cd = players[pId].cdMax;
					
					projectileHandler::newProjectile(utils::colors::colors[pId], {players[pId].getX(), players[pId].getY()}, { static_cast<float>(cos(pData.angle*3.14/180)), static_cast<float>(sin(pData.angle * 3.14 / 180)) });
				}
			}

    		world->Step(frameTimef, settings.m_velocityIterations, settings.m_positionIterations);

			frameTime = std::chrono::steady_clock::now() - t_begin;
			frameTimef = std::chrono::duration<float, std::ratio<1, 1>>(frameTime).count();
		}
	});

	server::api::init();
	server::api::recv();

	read.join();
}
