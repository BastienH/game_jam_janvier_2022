#include "projectile.hpp"

namespace server
{
    //Projectile
    void Projectile::move(const Point& d)
    {
        pos.x += d.x;
        pos.y += d.y;
    }

    namespace projectileHandler
    {
        std::unordered_map<unsigned short, Projectile> projectiles{};

        void update(const std::chrono::milliseconds& dt)
        {
            for (auto& [id, projectile] : projectiles)
            {
                //move proj
            }
        }


        Projectile& newProjectile(const utils::colors::Color& color, const Point& position, const Point& direction)
        {
            static unsigned id{ 0 };

            projectiles[id].pos = position;
            projectiles[id].direction = direction;
            projectiles[id].color = color;

            return projectiles[id++];
        }

        void removeProjectileById(unsigned short id)
        {
            projectiles.erase(id);
        }

        std::optional<std::reference_wrapper<server::Projectile>> getProjectileById(unsigned short id)
        {
            if (projectiles.find(id) == projectiles.end())
            {
                return std::nullopt;
            }
            return projectiles[id];
        }

        void updateProjectileById(unsigned short id, const Point& pos)
        {
            auto proj = getProjectileById(id);

            if (proj.has_value())
            {
                proj.value().get().pos = pos;
            }
        }

    }
}
