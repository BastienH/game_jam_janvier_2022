#include "player.hpp"

namespace server
{
    Player::Player(b2Vec2 pos, b2World *world) : _speed(1.f)
    {
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position = pos;
        _body = world->CreateBody(&bodyDef);

        _body->SetFixedRotation(true);
        _body->SetLinearDamping(0.f);
        b2MassData md;
        md.mass = 1.f;
        _body->SetMassData(&md);

        b2CircleShape circle;
        circle.m_radius = 0.3f;

        b2FixtureDef fd;
        fd.shape = &circle;

        _body->CreateFixture(&fd);
    }

    void Player::move(b2Vec2 dir)
    {
        dir.Normalize();

        b2Vec2 vel = _body->GetLinearVelocity();
        b2Vec2 desiredVel = _speed * dir;
        b2Vec2 velChange = desiredVel - vel;
        b2Vec2 impulse = _body->GetMass() * velChange;
        _body->ApplyLinearImpulseToCenter(impulse, true);
    }

    void Player::render()
    {
    }
} 
