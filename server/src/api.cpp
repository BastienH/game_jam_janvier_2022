#include "api.hpp"

#include <net/client.hpp>
#include <net/server.hpp>
#include <net/deserialise.hpp>

#include <map/point.hpp>
#include <protocol/protocol.hpp>


#include <memory>
#include <vector>

using namespace protocol;

namespace message
{
	using player = std::tuple<uint16_t, float, float, float, float, uint16_t>;
	using proj = std::tuple<uint16_t, float, float, uint16_t>;
	using playerInit = std::tuple<uint16_t>;
	using input = std::tuple<uint16_t, bool, bool, bool, bool, bool, float>;
}

namespace server::api
{
	using MyProt =
	Messages<
		Message<0, message::player>,
		Message<1, message::proj>,
		Message<2, message::playerInit>,
		Message<3, message::input>
	>;

	void received(message::input const& t) {
		auto pId = std::get<0>(t);
		auto & pData = p_datas[pId];
		pData.id = pId;
		std::tie(pId, pData.up, pData.down, pData.left, pData.right, pData.click, pData.angle) = t;
	}

	void received(message::player const& t) {
	}

	void received(message::proj const& t) {
	}

	void received(message::playerInit const& t) {
	}

	struct Handler {
		template<typename T>
		static void received(T const& t) { ::server::api::received(t); }
	};

	std::vector<InputPlayerData> p_datas;
	std::unique_ptr<net::Server> server{ nullptr };

	void init(std::uint16_t port)
	{
		std::printf("starting server... ");

		if (!net::init()) {
			std::fputs("cannot init network\n", stderr);
			return;
		}

		server = std::make_unique<net::Server>(port);

		std::printf("ready on %s:%d\n", server->ipAddr().c_str(), server->port());

	}

	void send()
	{
	}


	void recv()
	{
		std::atomic_bool running = true;

		try {
			while (running) {
				server->accept(
					[&running](net::Client client) {
						std::printf("client connected from %s:%d\n", client.ipAddr().c_str(), client.port());
						const auto pId = p_datas.size();
						p_datas.emplace_back();

						//Send protocol
						client.send(net::serialise(std::make_tuple(std::uint16_t(pId))));

						protocol::receiveMessageLoop<Handler, MyProt>(client, running);
						for(;;) sleep(1);
					}
				);
			}
		}
		catch (std::exception const& e) {
			if (running) throw e;
		}

		std::puts("disconnected");
	}
}
