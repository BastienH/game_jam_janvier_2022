#ifndef PLAYER_H
#define PLAYER_H

#include <chrono>
#include <stdint.h>

#include "box2d/box2d.h"

namespace server
{
    constexpr float player_cd_max{ 200 };

    class Player
    {
    private:
        b2Body *_body;
        float _speed;

    public:

        float cd{ 0 }, cdMax{ player_cd_max };

        Player(b2Vec2 pos, b2World *world);

        void move(b2Vec2 dir);
        void render();

        auto const & getX() const {
            return _body->GetPosition().x;
        }

        auto const & getY() const {
            return _body->GetPosition().y;
        }
    };
}

#endif // PLAYER_H