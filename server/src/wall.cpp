#include "wall.hpp"

Wall::Wall(b2Vec2 *vertexs, uint32_t count, b2World *world)
{
    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position = b2Vec2(0, -0);
    _body = world->CreateBody(&bodyDef);

    b2PolygonShape poly;
    poly.Set(vertexs, count);

    b2FixtureDef fd;
    fd.shape = &poly;

    _body->CreateFixture(&fd);
}

void Wall::render()
{
}
