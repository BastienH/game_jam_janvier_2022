#ifndef WALL_H
#define WALL_H

#include <stdint.h>

#include "box2d/box2d.h"

class Wall
{
private:
    b2Body *_body;

public:
    Wall(b2Vec2 *Vertexs, uint32_t count, b2World *world);

    void render();
};

#endif // WALL_H