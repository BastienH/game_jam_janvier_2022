#pragma once

#include <cstdint>
#include <colors.hpp>
#include <vector>

namespace server::api
{
	void init(std::uint16_t port = 4273);

	void send();

	void recv();

	
	struct InputPlayerData {
		unsigned id;
		bool up, down, left, right;
		bool click;
		float angle;
	};

	
	extern std::vector<InputPlayerData> p_datas;
}