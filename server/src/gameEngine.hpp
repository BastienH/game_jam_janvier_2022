#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <box2d/box2d.h>

#include "wall.hpp"
#include "player.hpp"

namespace GameEngine
{
    void init();

    void addWall(b2Vec2 *vertexs, uint32_t count);
    void addPlayer(b2Vec2 pos);

    void step(float dt);
    void render();
    void movePlayer(b2Vec2 dir);
};

#endif
