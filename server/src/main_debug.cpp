// MIT License

// Copyright (c) 2019 Erin Catto

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#define _CRT_SECURE_NO_WARNINGS
#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS 1

#ifdef DEBUG
#include "draw.hpp"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <SFML/OpenGL.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/VideoMode.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/String.hpp>
#endif

#include "settings.hpp"
#include "gameEngine.hpp"

#include <algorithm>
#include <stdio.h>
#include <thread>
#include <chrono>
#include <iostream>
#include <string>

#if defined(_WIN32)
#include <crtdbg.h>
#endif

#define CAMERA_WIDTH 1600
#define CAMERA_HEIGHT 900

std::unique_ptr<sf::RenderWindow> window{nullptr};

Settings settings;

void addEntities();

void glfwErrorCallback(int error, const char *description)
{
	std::cout << "GLFW error occured. Code: " << error << ". Description: " << description << std::endl;
}

static void ResizeWindowCallback(GLFWwindow *, int width, int height)
{
	g_camera.m_width = width;
	g_camera.m_height = height;
}

static void KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		switch (key)
		{
		case GLFW_KEY_ESCAPE:
			// Quit
			//glfwSetWindowShouldClose(g_mainWindow, GL_TRUE);
			break;

		case GLFW_KEY_UP:
			GameEngine::movePlayer(b2Vec2(0, 1));
			break;
		case GLFW_KEY_DOWN:
			GameEngine::movePlayer(b2Vec2(0, -1));
			break;
		case GLFW_KEY_RIGHT:
			GameEngine::movePlayer(b2Vec2(1, 0));
			break;
		case GLFW_KEY_LEFT:
			GameEngine::movePlayer(b2Vec2(-1, 0));
			break;

		case GLFW_KEY_SPACE:
			break;

		default:
			// GAME KEY DOWN
			break;
		}
	}
	else if (action == GLFW_RELEASE)
	{
		// GAME KEY UP
		switch (key)
		{
		case GLFW_KEY_UP:
		case GLFW_KEY_DOWN:
		case GLFW_KEY_RIGHT:
		case GLFW_KEY_LEFT:
			GameEngine::movePlayer(b2Vec2(0, 0));
			break;
		}
	}
	// else GLFW_REPEAT
}

static void CharCallback(GLFWwindow *window, unsigned int c)
{
}

static void MouseButtonCallback(GLFWwindow *window, int32 button, int32 action, int32 mods)
{
	double xd, yd;
	//glfwGetCursorPos(g_mainWindow, &xd, &yd);
	b2Vec2 ps((float)xd, (float)yd);

	// Use the mouse to move things around.
	if (button == GLFW_MOUSE_BUTTON_1)
	{
		//<##>
		//ps.Set(0, 0);
		b2Vec2 pw = g_camera.ConvertScreenToWorld(ps);
		if (action == GLFW_PRESS)
		{
			// GAME MOUSE DOWN
		}
		else if (action == GLFW_RELEASE)
		{
			// GAME MOUSE UP
		}
	}
	else if (button == GLFW_MOUSE_BUTTON_2)
	{
		if (action == GLFW_PRESS)
		{
		}
		else if (action == GLFW_RELEASE)
		{
		}
	}
}

static void MouseMotionCallback(GLFWwindow *, double xd, double yd)
{
	b2Vec2 ps((float)xd, (float)yd);

	b2Vec2 pw = g_camera.ConvertScreenToWorld(ps);

	// GAME MOUSE MOTION

	/*
	if (s_rightMouseDown)
	{
		b2Vec2 diff = pw - s_clickPointWS;
		g_camera.m_center.x -= diff.x;
		g_camera.m_center.y -= diff.y;
		s_clickPointWS = g_camera.ConvertScreenToWorld(ps);
	}
	*/
}

static void ScrollCallback(GLFWwindow *window, double dx, double dy)
{
	if (dy > 0)
	{
		g_camera.m_zoom /= 1.1f;
	}
	else
	{
		g_camera.m_zoom *= 1.1f;
	}
}

//
int main(int, char **)
{
	char buffer[128] = {0};

	g_camera.m_width = CAMERA_WIDTH;
	g_camera.m_height = CAMERA_HEIGHT;

	// Register error callback first
	glfwSetErrorCallback(glfwErrorCallback);

	if (glfwInit() == GLFW_FALSE)
	{
		std::cout << "glfw init error" << std::endl;
		exit(1); // or handle the error in a nicer way
	}

	window = std::make_unique<sf::RenderWindow>(sf::VideoMode(g_camera.m_width, g_camera.m_height), buffer);
	window->setActive(true);

	//glfwMakeContextCurrent(window->);

	// Load OpenGL functions using glad
	int version = gladLoadGL(); //glfwGetProcAddress);
	printf("GL %d.%d\n", GLFW_VERSION_MAJOR, GLFW_VERSION_MINOR);
	printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));

	g_debugDraw.Create();

	glClearColor(0.2, 0.2, 0.2, 1.0);

	GameEngine::init();
	addEntities();

	static std::chrono::steady_clock::time_point t_begin = std::chrono::steady_clock::now();
	std::chrono::duration<double> frameTime(0);
	float frameTimef = 0;

	while (window->isOpen())
	{
		t_begin = std::chrono::steady_clock::now();

		//glfwGetWindowSize(g_mainWindow, &g_camera.m_width, &g_camera.m_height);
		g_camera.m_center = b2Vec2(0, 0);
		g_camera.m_zoom = 10;
		/*
		int bufferWidth, bufferHeight;
		glfwGetFramebufferSize(g_mainWindow, &bufferWidth, &bufferHeight);
		glViewport(0, 0, bufferWidth, bufferHeight);
		*/

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window->close();

				continue;
			}
		}

		window->clear(sf::Color::Black);
		// GAME STEP
		GameEngine::step(frameTimef);
		GameEngine::render();

		window->display();

		// Throttle to cap at 60Hz. This adaptive using a sleep adjustment. This could be improved by
		// using mm_pause or equivalent for the last millisecond.
		std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
		std::chrono::duration<double> target(1.0 / 60.0);
		std::chrono::duration<double> timeUsed = t2 - t_begin;
		std::chrono::duration<double> sleepTime = target - timeUsed;
		if (sleepTime > std::chrono::duration<double>(0))
		{
			std::this_thread::sleep_for(sleepTime);
		}

		frameTime = std::chrono::steady_clock::now() - t_begin;
		frameTimef = std::chrono::duration<float, std::ratio<1, 1>>(frameTime).count();
		/*
		std::string str = "FPS : " + std::to_string(1.0f / frameTimef);
		sf::String title = str;
		window->setTitle(title);
		*/
	}

	return 0;
}

void addEntities()
{
	b2Vec2 vertexs1[] = {b2Vec2(-3, -3), b2Vec2(-2, -3), b2Vec2(-2, 3), b2Vec2(-3, 3)};
	GameEngine::addWall(vertexs1, 4);
	b2Vec2 vertexs2[] = {b2Vec2(3, -3), b2Vec2(2, -3), b2Vec2(2, 3), b2Vec2(3, 3)};
	GameEngine::addWall(vertexs2, 4);

	b2Vec2 player1Pos(0, 0);
	GameEngine::addPlayer(player1Pos);
}
