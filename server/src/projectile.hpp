#pragma once

#include <optional>
#include <functional>
#include <unordered_map>
#include <chrono>

#include <map/point.hpp>
#include <colors.hpp>

namespace server
{
    constexpr float proj_speed{ 1 };
    constexpr float proj_radius{ 5 };

    struct Projectile
    {
        Point pos{ 0, 0 };
        Point direction{ 0, 0 };
        utils::colors::Color color{0, 0, 0};

        void move(const Point& d);
    };

    namespace projectileHandler
    {
        void update(const std::chrono::milliseconds& dt);

        Projectile& newProjectile(const utils::colors::Color& color, const Point& position, const Point& direction);
        void removeProjectileById(unsigned short id);
        std::optional<std::reference_wrapper<server::Projectile>> getProjectileById(unsigned short id);
        void updateProjectileById(unsigned short id, const Point& pos);
    };
}
