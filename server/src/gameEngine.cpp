#include "gameEngine.hpp"

#include <iostream>
#include <vector>

#ifdef DEBUG
#include "draw.hpp"
#endif
#include "settings.hpp"

extern Settings settings;

using namespace GameEngine;
using namespace server;

b2World *_world;
std::vector<Wall> _walls;
std::vector<Player> _players;

void GameEngine::init()
{
    b2Vec2 gravity;
    gravity.Set(0.0f, 0.0f);
    _world = new b2World(gravity);

    b2BodyDef bd;
    b2Body *ground = _world->CreateBody(&bd);

    b2EdgeShape shape;

    float rt = 16.f / 9.f;
    // Floor
    shape.SetTwoSided(b2Vec2(rt * -4.f, -4.f), b2Vec2(rt * 4.f, -4.f));
    ground->CreateFixture(&shape, 0.0f);
    // Left wall
    shape.SetTwoSided(b2Vec2(rt * -4.f, -4.f), b2Vec2(rt * -4.f, 4.f));
    ground->CreateFixture(&shape, 0.0f);
    // Right wall
    shape.SetTwoSided(b2Vec2(rt * 4.f, -4.f), b2Vec2(rt * 4.f, 4.f));
    ground->CreateFixture(&shape, 0.0f);
    // Roof
    shape.SetTwoSided(b2Vec2(rt * -4.f, 4.f), b2Vec2(rt * 4.f, 4.f));
    ground->CreateFixture(&shape, 0.0f);

#ifdef DEBUG
    _world->SetDebugDraw(&g_debugDraw);
#endif
}

void GameEngine::addWall(b2Vec2 *vertexs, uint32_t count)
{
    _walls.push_back(Wall(vertexs, count, _world));
}

void GameEngine::addPlayer(b2Vec2 pos)
{
    _players.push_back(Player(pos, _world));
}

void GameEngine::step(float dt)
{
    _world->Step(dt, settings.m_velocityIterations, settings.m_positionIterations);
}

void GameEngine::render()
{
#ifdef DEBUG
    uint32 flags = 0;
    flags += settings.m_drawDebug * b2Draw::e_shapeBit;
    /*
    flags += settings.m_drawDebug * b2Draw::e_jointBit;
    flags += settings.m_drawDebug * b2Draw::e_aabbBit;
    flags += settings.m_drawDebug * b2Draw::e_centerOfMassBit;
    */
    g_debugDraw.SetFlags(flags);
    _world->DebugDraw();
    g_debugDraw.Flush();
#endif
}

void GameEngine::movePlayer(b2Vec2 dir)
{
    _players.at(0).move(dir);
}
